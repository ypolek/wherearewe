package ypolek.wherearewe.map;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.HashMap;
import java.util.Map;

import ypolek.wherearewe.db.DbHelper;
import ypolek.wherearewe.db.OnUserListChangedListener;
import ypolek.wherearewe.model.User;

public class MapFragment extends SupportMapFragment implements OnMapReadyCallback {

    public static final String ARG_SELECTED_USER = "arg_selected_user";

    private static final float ZOOM_LEVEL = 16f;

    private GoogleMap map;
    private Map<User, Marker> markers = new HashMap<>();

    private User selectedUser;

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);

        Bundle args = getArguments();
        if (args != null && args.containsKey(ARG_SELECTED_USER)) {
            selectedUser = args.getParcelable(ARG_SELECTED_USER);
        }

        getMapAsync(this);

        DbHelper dbHelper = new DbHelper(getContext());
        dbHelper.subscribeToUserListChanges(new OnUserListChangedListener() {
            @Override
            public void onUserAdded(User user) {
                addMarker(user);
            }

            @Override
            public void onUserRemoved(User user) {
                removeMarker(user);
            }

            @Override
            public void onUserChanged(User user) {
                updateMarker(user);
            }
        });
    }

    private void addMarker(User user) {
        // Do not show marker if user's location is unknown
        if (user.longitude == 0.0 && user.latitude == 0.0) return;

        Marker marker = null;
        if (map != null) {
            marker = map.addMarker(new MarkerOptions()
                    .position(new LatLng(user.latitude, user.longitude))
                    .title(user.name));
        }
        markers.put(user, marker);
    }

    private void removeMarker(User user) {
        Marker marker = markers.remove(user);
        if (marker != null) {
            marker.remove();
        }
    }

    private void updateMarker(User user) {
        Marker marker = markers.get(user);
        if (marker != null) {
            marker.setPosition(new LatLng(user.latitude, user.longitude));
        }
        markers.put(user, marker);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        map = googleMap;
        if (ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            map.setMyLocationEnabled(true);
        }
        createMarkersIfRequired();
        if (selectedUser != null) {
            showUser(selectedUser);
        }
    }

    private void createMarkersIfRequired() {
        for (User user : markers.keySet()) {
            if (markers.get(user) == null) {
                addMarker(user);
            }
        }
    }

    public void showUser(User user) {
        if (map != null) {
            map.animateCamera(CameraUpdateFactory.newLatLngZoom(
                    new LatLng(user.latitude, user.longitude),
                    ZOOM_LEVEL));
            selectedUser = null;
        } else {
            selectedUser = user;
        }
    }
}
