package ypolek.wherearewe.map;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;

import ypolek.wherearewe.R;

public class MapActivity extends FragmentActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getResources().getBoolean(R.bool.dual_pane)) {
            finish();
            return;
        }

        if (savedInstanceState == null) {
            MapFragment map = new MapFragment();
            map.setArguments(getIntent().getExtras());
            getSupportFragmentManager().beginTransaction().add(android.R.id.content, map).commit();
        }
    }
}
