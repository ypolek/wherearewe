package ypolek.wherearewe.db;

import ypolek.wherearewe.model.User;

public interface OnUserListChangedListener {
    void onUserAdded(User user);
    void onUserRemoved(User user);
    void onUserChanged(User user);
}
