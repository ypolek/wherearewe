package ypolek.wherearewe.db;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Build;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.HashMap;
import java.util.Map;

import ypolek.wherearewe.model.User;

public class DbHelper {

    private static final String TAG = DbHelper.class.getSimpleName();
    private static final String KEY_USER_ID = "key_user_id";
    private static final String USERS = "users";

    private Context context;
    private DatabaseReference dbRoot;

    private UserEventListener currentUserEventListener;

    public DbHelper(Context context) {
        this.context = context;
        dbRoot = FirebaseDatabase.getInstance().getReference();

        // Create new user if it does not exists yet
        getUserIdOrCreateNewUser();
    }

    public String getUserIdOrCreateNewUser() {
        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(context);
        String userId = sp.getString(KEY_USER_ID, null);
        if (userId == null) {
            userId = writeNewUser(Build.MANUFACTURER + " " + Build.MODEL);
            sp.edit().putString(KEY_USER_ID, userId).apply();
        }
        return userId;
    }

    /**
     * Writes new user into DB and returns user ID
     * @param name user name
     * @return user's unique ID
     */
    private String writeNewUser(String name) {
        DatabaseReference newUserRef = dbRoot.child(USERS).push();
        String uid = newUserRef.getKey();
        newUserRef.setValue(new User(uid, name));
        return uid;
    }

    public void writeLocation(Location location) {
        Map<String, Object> childUpdates = new HashMap<>();
        childUpdates.put("/longitude", location.getLongitude());
        childUpdates.put("/latitude", location.getLatitude());
        dbRoot.child(USERS).child(getUserIdOrCreateNewUser()).updateChildren(childUpdates);
    }

    public void subscribeToUserListChanges(OnUserListChangedListener listener) {

        DatabaseReference usersRef = dbRoot.child(USERS);

        if (currentUserEventListener != null) {
            usersRef.removeEventListener(currentUserEventListener);
        }

        currentUserEventListener = new UserEventListener(getUserIdOrCreateNewUser(), listener);
        usersRef.addChildEventListener(currentUserEventListener);
    }

    public void unsubscribeFromUserListChanges() {
        dbRoot.child(USERS).removeEventListener(currentUserEventListener);
        currentUserEventListener = null;
    }

    private static class UserEventListener implements ChildEventListener {

        private String ownId;
        private OnUserListChangedListener listener;

        public UserEventListener(String ownId, OnUserListChangedListener listener) {
            this.ownId = ownId;
            this.listener = listener;
        }

        @Override
        public void onChildAdded(DataSnapshot dataSnapshot, String previousChildName) {
            User user = readUser(dataSnapshot);
            if (!user.id.equals(ownId)) {
                listener.onUserAdded(user);
            }
        }

        @Override
        public void onChildChanged(DataSnapshot dataSnapshot, String previousChildName) {
            User user = readUser(dataSnapshot);
            if (!user.id.equals(ownId)) {
                listener.onUserChanged(user);
            }
        }

        @Override
        public void onChildRemoved(DataSnapshot dataSnapshot) {
            User user = readUser(dataSnapshot);
            if (!user.id.equals(ownId)) {
                listener.onUserRemoved(user);
            }
        }

        @Override
        public void onChildMoved(DataSnapshot dataSnapshot, String previousChildName) {}

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.w(TAG, "Failed to retrieve user list", databaseError.toException());
        }

        private User readUser(DataSnapshot dataSnapshot) {
            User user = dataSnapshot.getValue(User.class);
            user.id = dataSnapshot.getKey();
            return user;
        }
    }
}
