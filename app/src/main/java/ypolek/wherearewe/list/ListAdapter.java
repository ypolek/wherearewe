package ypolek.wherearewe.list;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import ypolek.wherearewe.R;
import ypolek.wherearewe.model.User;

public class ListAdapter extends RecyclerView.Adapter<ListItemViewHolder> {

    private List<User> users = new ArrayList<>();
    private OnUserSelectedListener clickListener;

    public ListAdapter(OnUserSelectedListener clickListener) {
        this.clickListener = clickListener;
    }

    public void addUser(User user) {
        int position = getItemPosition(user);
        if (position < 0) {
            users.add(user);
            notifyDataSetChanged();
        } else {
            users.set(position, user);
            notifyItemChanged(position);
        }
    }

    public void removeUser(User user) {
        users.remove(user);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    @Override
    public ListItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item, parent, false);
        return new ListItemViewHolder(itemView, clickListener);
    }

    @Override
    public void onBindViewHolder(ListItemViewHolder holder, int position) {
        holder.setUser(getItemAtPosition(position));
    }

    private User getItemAtPosition(int position) {
        return users.get(position);
    }

    private int getItemPosition(User user) {
        for (int i=0; i<users.size(); i++) {
            if (users.get(i).id.equals(user.id)) return i;
        }
        return -1;
    }
}
