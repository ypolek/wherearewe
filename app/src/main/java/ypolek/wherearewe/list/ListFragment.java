package ypolek.wherearewe.list;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import ypolek.wherearewe.R;
import ypolek.wherearewe.db.DbHelper;
import ypolek.wherearewe.db.OnUserListChangedListener;
import ypolek.wherearewe.model.User;

public class ListFragment extends Fragment {

    private DbHelper dbHelper;
    private ListAdapter adapter;
    private RecyclerView recyclerView;
    private View emptyView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        dbHelper = new DbHelper(getContext());
        Activity activity = getActivity();
        if (activity instanceof OnUserSelectedListener) {
            adapter = new ListAdapter((OnUserSelectedListener) activity);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.list_fragment, container, false);

        recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerView.addItemDecoration(new DividerItemDecoration(getContext()));
        recyclerView.setAdapter(adapter);

        emptyView = view.findViewById(R.id.empty_view);
        updateEmptyViewVisibility();

        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        dbHelper.subscribeToUserListChanges(new OnUserListChangedListener() {
            @Override
            public void onUserAdded(User user) {
                adapter.addUser(user);
                updateEmptyViewVisibility();
            }

            @Override
            public void onUserRemoved(User user) {
                adapter.removeUser(user);
                updateEmptyViewVisibility();
            }

            @Override
            public void onUserChanged(User user) {}
        });
    }

    @Override
    public void onPause() {
        super.onPause();

        dbHelper.unsubscribeFromUserListChanges();
    }

    private void updateEmptyViewVisibility() {
        boolean isEmpty = adapter.getItemCount() <= 0;
        recyclerView.setVisibility(isEmpty ? View.GONE : View.VISIBLE);
        emptyView.setVisibility(isEmpty ? View.VISIBLE : View.GONE);
    }
}
