package ypolek.wherearewe.list;

import ypolek.wherearewe.model.User;

public interface OnUserSelectedListener {
    void onUserSelected(User user);
}
