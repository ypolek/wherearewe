package ypolek.wherearewe.list;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import ypolek.wherearewe.R;
import ypolek.wherearewe.model.User;

public class ListItemViewHolder extends RecyclerView.ViewHolder {

    private final TextView name;
    private User user;

    public ListItemViewHolder(View itemView, final OnUserSelectedListener clickListener) {
        super(itemView);

        name = (TextView) itemView.findViewById(R.id.name);

        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                clickListener.onUserSelected(user);
            }
        });
    }

    public void setUser(User user) {
        this.user = user;
        name.setText(user.name);
    }
}
